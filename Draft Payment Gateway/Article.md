# Draft IDE

## Table of Contents

  - [Table of Contents](#table-of-contents)
  - [Payment Gateway](#payment-gateway)
    - [Flow](#flow)
      - [Sale Order Flow](#sale-order-flow)
      - [eCommerce Website Flow](#ecommerce-website-flow)
    - [Activity Diagram](#activity-diagram)
      - [Sale Order Flow](#sale-order-flow-1)
      - [eCommerce Website Flow](#ecommerce-website-flow-1)

## Payment Gateway

- Integrate with Odoo Sale Order, eCommerce Website,and maybe POS with QRIS only to provide another payment than Cash or Debit/Credit Card.
- Online Payment, Payment Status Tracking

### Flow

#### Sale Order Flow

1. Salesperson create SaleOrder
2. When create Invoice, it should automatically generate payment link, and send invoice through email with generated payment link.
3. Customer can choose what payment method they want to use using generated payment link. 
4. Odoo create new transaction request Payment Gateway Provider using payment method they choose and INVOICE as merchant reference.
5. Then from generated payment link, it should create new page with instruction how to pay the invoice.
6. When customer have paid the invoice, Payment Gateway will confirm the payment, and sent notification to callback url.
7. Odoo receive notification from callback url, check the status, and change the invoice as PAID if the status is PAID.
8. Customer redirected to PAID Invoice, and sent new paid invoice to their email.


#### eCommerce Website Flow

1. Customer go to website, Shop, Add to cart some items, then "Process Checkout"
2. When customer on a Payment Page, they can choose what payment method they want to use (Bank VA, QRIS, or Convenience Store). Then they can click "Pay Now"
3. System will automatically move it to "sale order" stage, create Invoice, and sent new transaction request to Payment Gateway with invoice reference as merchant_ref and payment method customer choose.
4. Odoo will receive response with pay_code and expiry time, sent email to customer with invoice and instruction to pay.
5. Current customer page will redirected to UNPAID invoice and instruction to pay the invoice.
6. When customer have paid the invoice, Payment Gateway will confirm the payment and sent notification to callback url.
7. Odoo receive notification from callback url, check the status, and change the invoice as PAID if the status is PAID.
8. Odoo sent email to customer with PAID Invoice.

### Activity Diagram

#### Sale Order Flow

![Sale Order Flow](img/sale-order-flow.jpg)

#### eCommerce Website Flow

![eCommerce Website Flow](img/sale-order-flow.jpg)