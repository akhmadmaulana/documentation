# Install Odoo 13 on macOS

## Requirements
- CommandLineTools/xcode (for installing homebrew)
- Homebrew (for installing postgresql and other python3 version)
- PostgreSQL (install with homebrew, or you can use Docker too)
- python3.7 (pillow 6.1.0 dependencies only supported by [python3.5-3.7](https://pillow.readthedocs.io/en/stable/installation.html#python-support))

## Steps
### Preparing
1. Install command-line tools with command

    ```sh
    xcode-select --install
    ```

    and there should show new window like this

    ![xcode-select --install](img/xcode-select-install.png)

    Click "Install" > Agree EULA (or if you want to read it), it will download CommandLineTools and wait until installed, and Done.
2. Next is install [homebrew](https://brew.sh) with command
   
    ```sh
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    ```

    it will take time, around 500MB-1GB pulled from their repository.

3. Next is installing python3.7 with homebrew. Actually, CommandLineTools have installed python3 too, but usually it is the latest version (on macOS 10.15.7, it is python3.8). Since we want to run Odoo13, and Odoo13 have `Pillow v6.1.0` as dependencies and it can only run from python3.5-3.7, so we can't run it with Python3.8 provided by CommandLineTools. Then we need to install python3.7 with homebrew
   
    ```sh
    brew install python@3.7
    ```

    When it done, you can create symlink using command

    ```sh
    brew link python@3.7
    ```

    now you have `python3.7` command enable to run.

    ![symlink python@3.7](img/symlink.png)

    or if you want your `python3` and `pip3` command use `python3.7`, you need to restart the terminal.
4. Install virtualenv using module pip
   
    ```sh
    python3.7 -m pip install virtualenv
    ```

    ![virtualenv python@3.7](img/venv.png)

### Setup PostgreSQL
1. install postgresql first with command
2. 
    ```sh
    brew install postgresql
    ```

    command above will install latest version of postgresql. If you want to install previous version, use command `brew search` first like below, then use `brew link` to use that version

    ```sh
    ➜  ~ brew search postgresql
    ==> Formulae
    postgresql ✔        postgresql@11       postgresql@9.4      postgresql@9.6      postgrest
    postgresql@10       postgresql@12       postgresql@9.5      qt-postgresql
    ==> Casks
    navicat-for-postgresql
    ➜  ~
    ```

    ```sh
    brew install postgresql@11
    brew link postgresql@11
    ```

3. Run postgresql as service with `brew services`
   
    ```sh
    ➜  ~ brew services start postgresql
    ==> Successfully started `postgresql` (label: homebrew.mxcl.postgresql)
    ➜  ~ brew services
    Name       Status  User   Plist
    postgresql started akhmad /Users/akhmad/Library/LaunchAgents/homebrew.mxcl.postgresql.plist
    ➜  ~
    ```

4. Create user using command `createuser`
   
   ```sh
   createuser odootes
   ```

   you can add flag `--pwprompt` if you want to create password for the user.
5. Add user `odootes` permission to create database
   
   ```sh
   psql postgres
   ALTER USER odootes CREATEDB;
   ```

### Odoo Installation from Source
1. Clone Odoo 13 from github repository
   
    ```sh
    git clone https://github.com/odoo/odoo --branch 13.0 --depth 1 odoo13
    ```

    ![clone odoo](img/clone-odoo.png)

2. Clone odoo 13 enterprise from github repository
    Change directory to odoo13 that created fron step 1, then clone enterprise repository in there

    ```sh
    cd odoo13
    git clone https://github.com/odoo/enterprise --branch 13.0 --depth 1 --single-branch enterprise
    ```

3. Create virtualenv inside odoo directory
   
    ```sh
    ls -al
    cd odoo13
    virtualenv -p /usr/local/opt/python@3.7/bin/python3.7 venv
    ls
    ```

    Now you have venv directory created

    ![venv directory](img/venv-created.png)

4. Activate venv
5. 
    ```sh
    source venv/bin/activate
    ```

    Now you are in a virtualenv mode. While in this mode, your `python` and `pip` command will be linked to `python3.7` and `pip3.7` as we use flag `-p` to use `python3.7`

6. Install setuptools and wheel, and requirements.txt with pip, but first confirm pip use python3.7
7. 
    ```sh
    (venv) ➜  odoo13 git:(13.0) pip -V
    pip 21.2.2 from /Users/akhmad/Workspaces/odoo/odoo13/venv/lib/python3.7/site-packages/pip (python 3.7)
    (venv) ➜  odoo13 git:(13.0) pip install setuptools wheel && pip install -r requirements.txt 
    ```

8. Once done, you can create file with extension `.conf` and you can see [here for other configuration](https://www.odoo.com/documentation/13.0/developer/misc/other/cmdline.html). Below is example for `.conf`
   
   ```conf
   [options]
   db_name = False
   db_user = odootes
   db_password = False
   addons_path = addons, odoo/addons, enterprise, custom-addons
   list_db = True
   dev = all
   ```

   you can add your addons directory path to `addons_path`, the first two (`addons` and `odoo/addons`) is a base odoo module you can get from community version. `enterprise` is a directory we cloned on a step 2 above. And `custom-addons` is a directory manually created to put all of our custom module. This path is relative to `conf` file, so the structure would be like this.

   ```
    .
    ├── addons
    │   ├── account
    │   ├── account_analytic_default
    │   └── ...
    ├── custom-addons
    │   └── pcs_pos_salesperson
    ├── ...
    ├── enterprise
    │   ├── account_3way_match
    │   ├── account_accountant
    │   └── ...
    ├── odoo
    │   ├── addons
    │   ├── cli
    │   └── ...
    ├── odoo-bin
    ├── odoo.conf
    ├── ...
    └── venv
        ├── ...
        └── share
   ```

9.  Now it start to run odoo
    
   ```sh
   (venv) ➜  odoo13 git:(13.0) ✗ python odoo-bin -c odoo.conf
    ```
    
    And odoo will run on port `:8069` as a default. If you want to use another port, you can add `xmlrpc_port = <port>` to .conf file.
    If it's the first time odoo run, or the user don't have any database, it will redirected to page like this

    ![Odoo CreateDB](img/odoo-create-db.png)

    If you have created database for the user, you can specify `db_name` used in a conf or using flag `-d <dbname>`. Then you can pass flag `-i base` and the database will be initialized with module `base`.

### Logging
From the link [odoo-bin here](https://www.odoo.com/documentation/13.0/developer/misc/other/cmdline.html#logging), you can see there is **Logging** section. By default, odoo will display logging to stdout (which mean terminal) and only show level INFO or WARNING. Here you can specify what error you want to display and where you want to save it.

For displaying other log level, you can add flag `--log-level <level>` which level you can use is `['critical', 'error', 'warn', 'debug']`, or you can add this to `.conf` file with the name `log_level = error`. Or you can save it to database using flag `--log-db <dbname>` or conf `log_db = <dbname>`. For other options, you can see link logging above.
