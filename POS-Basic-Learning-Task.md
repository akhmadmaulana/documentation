# [28.9.1] POS Cashier/Salesperson

## Create custom module
1. First is create new custom module to extend point_of_sale module. We can do this with `odoo-bin scaffold` command
   ```sh
   # python odoo-bin scaffold <module_name> <directory_where_it_created>
   $ python odoo-bin scaffold pcs_pos_salesperson custom-addons
   ```

2. Then add new directory called "static" in the module root directory, and add other directories so the structure should look like below
   ```sh
   └── static
        └── src
            ├── css
            ├── js
            └── xml
   ```

## [Task 1] Allow to input both Cashier and Salesperson to pos.ui Order
We need to add new Salesperson to show on UI, POS Order, and Receipt. And we need to move the button from far-right, closer to logo.
1. First we add new file called `salesperson_widget.js` and put it inside `/static/src/js` to create new Salesperson widget, and xml file called `pos_salesperson.xml` inside `static/src/xml` for the structure where we want to load the widget. Then add `pos_salesperson.xml` to `__manifest__.py` and `salesperson_widget.js` to templates on directory `views` and extend `point_of_sale.assets` and load it on `__manifest__.py`. So the structure will be
   
   ```sh
    ├── __manifest__.py
    ├── static
    │   └── src
    │       ├── css
    │       ├── js
    │       │   └── salesperson_widget.js
    │       └── xml
    │           └── pos_salesperson.xml
    └── views
        ├── pos_salesperson_frontend_templates.xml
   ```

   and fill `pos_salesperson_frontend_templates.xml` with
   
   ```xml
   <odoo>
     <template id="pos_salesperson_frontend" inherit_id="point_of_sale.assets">
      <xpath expr="." position="inside">
        <script type="text/javascript" src="/pcs_pos_salesperson/static/src/js/salesperson_widget.js" />
      </xpath>
     </template>
   </odoo>
   ```

   and add file inside views to `data`, and inside `static/src/xml/` to `qweb` on  `__manifest__.py`
   
   ```py
    'data': [
        'views/pos_salesperson_frontend_templates.xml',
    ],
    'qweb': [
        'static/src/xml/pos_salesperson.xml',
    ]
   ```

2. Next is we create new widget, add this code to file `static/src/js/salesperson_widget.js`
   
   ```js
    odoo.define('point_of_sale.salesperson_widget', function (require) {
      "use strict";

      var chrome = require('point_of_sale.chrome');
      var PosBaseWidget = require('point_of_sale.BaseWidget');
      var core = require('web.core');
      var _t = core._t;

      var SalespersonWidget = PosBaseWidget.extend({
          template: 'SalespersonWidget',
          init: function (parent, options) {
              options = options || {};
              this._super(parent, options);
          },
          renderElement: function () {
              var self = this;
              this._super();
              this.$el.click(function () {
                  self.click_salesperson();
              });
          },
          click_salesperson: function () {
              if(!this.pos.config.module_pos_hr) { return; }
              var self = this;
              this.gui.select_salesperson({
                  'security': true,
                  'current_employee': this.pos.get_salesperson(),
                  'title': _t('Change Salesperson'),
              }).then(function(employee){
                  self.pos.set_salesperson(employee);
                  self.chrome.widget.salesperson.renderElement();
                  self.renderElement();
              });
          },
          get_name: function () {
              var user = this.pos.get_salesperson();
              if (user) {
                  return user.name;
              } else {
                  return "";
              }
          }
      });

      chrome.Chrome.include({
          build_widgets: function () {
              this.widgets.push({
                  'name': 'salesperson',
                  'widget': SalespersonWidget,
                  'replace': '.placeholder-SalespersonWidget',
              });
              this._super();
          }
      })

      return {
          'SalespersonWidget': SalespersonWidget,
      };
    });
   ```

   On the code above, we create new widget called `SalespersonWidget`. This widget will extend renderElement function to add `click handler` when the button clicked, and create new function to get salesperson name and function when the button clicked. Then on the bottom, we add this widget to widgets list on Chrome module.

   We can see there is `'replace': 'placeholder-SalespersonWidget'` when we add the widget. This value is a identifier to an element from file `/static/src/xml/pos_salesperson.xml`. We can place this widget anywhere, when it detect there is element with class `placeholder-SalespersonWidget`, it will be replaced by this widget.
3. Next is we create where we want to place the widget, go to file `static/src/xml/pos_salesperson.xml`, and add with this code
  
   ```xml
   <template id="template" xml:space="preserve">

    <!-- Change cashier and salesperson positition to left of the topheader -->
    <t t-extend="Chrome">
      <t t-jquery=".pos > .pos-topheader" t-operation="replace">
        <div class="pos-topheader">
          <div class="pos-branding">
            <img class="pos-logo" src="/point_of_sale/static/src/img/logo.png" alt="Logo"/>
            <div class="pos-top-rightheader">
              <div class="oe_status">
                <span class="placeholder-UsernameWidget"></span>
              </div>
              <div class="oe_status">
                <span class="placeholder-SalespersonWidget"></span>
              </div>
            </div>
          </div>
          <div class="pos-rightheader">
            <span class="placeholder-OrderSelectorWidget"></span>
          </div>
        </div>
      </t>
    </t>

    <t t-name="SalespersonWidget">
      <label>Salesperson</label>
      <span class="salesperson">
          <t t-esc="widget.get_name()" />
      </span>
    </t>

    <t t-name="UsernameWidget">
      <label>Cashier</label>
      <span class="cashier">
          <t t-esc="widget.get_name()" />
      </span>
    </t>
   </template>
 
   ```
   We want to change position from far-right top-header, to closer with logo. So we need to extend template `Chrome`, and find element with class pos-topheader, and replace the element. Then we move cashier and add salesperson to pos-branding which is the logo. You can see there is a template with theier t-name, t-name should be same as the widget from widgets list. And you can call function the widget have.
4. For styling, we create new file inside `static/src/css/` with name `pos_salesperson_custom.css`, then we can copy it from style of pos-rightheader, and change some value like the width. Then don't forget to add css file to the `/views/pos_salesperson_frontend_templates.xml`
  
   ```css
    .pos .pos-topheader .pos-branding .pos-top-rightheader {
      position: absolute;
      right: 0;
      top: 0;
      height: 100%;
      display: -webkit-flex;
      display: flex;
      overflow: hidden;
      overflow-x: auto;
      -webkit-overflow-scrolling: touch;
    }
    .pos .pos-topheader .pos-branding .pos-top-rightheader > * {
      border-right: 1px solid #292929;
      border-left: 1px solid #292929;
    }

    .pos .pos-topheader .pos-branding .pos-top-rightheader .header-button {
      float: right;
      height: 48px;
      padding-left: 16px;
      padding-right: 16px;
      border-right: 1px solid #292929;
      border-left: 1px solid #292929;
      color: #ddd;
      line-height: 48px;
      text-align: center;
      cursor: pointer;

      -webkit-transition-property: background;
      -webkit-transition-duration: 0.2s;
      -webkit-transition-timing-function: ease-out;
    }
    .pos
      .pos-topheader
      .pos-branding
      .pos-top-rightheader
      .header-button:last-child {
      border-left: 1px solid #3a3a3a;
    }
    .pos .pos-topheader .pos-branding .pos-top-rightheader .header-button:active {
      background: rgba(0, 0, 0, 0.2);
      color: #eee;
    }
    .pos .pos-topheader .pos-branding .pos-top-rightheader .header-button.confirm {
      background: #359766;
      color: white;
      font-weight: bold;
    }

    .pos-top-rightheader .oe_status > label {
      position: absolute;
      top: 0;
      font-size: 12px;
      padding-bottom: 3px;
    }

    .pos-top-rightheader .oe_status {
      padding: 18px;
    }

    .pos-top-rightheader .oe_status:hover {
      color: rgba(255, 255, 255, 0.8);
    }
   ```

5. After finishing step 4, you can see the widget loaded, but there is no salesperson and there will be an error if we click it. It is because we don't have function to `select_salesperson()` in gui module and `get_salesperson` in models, so we need to extend them too, first we create files `salesperson_db.js`, `salesperson_model.js`, and `salesperson_gui.js` inside `static/src/js/` directory. then we fill it with
  
  ```js
  // salesperson_db.js
  odoo.define('point_of_sale.salesperson_db', function (require) {
      "use strict";

      var PosDB = require('point_of_sale.DB');

      PosDB.include({
          set_salesperson: function (salesperson) {
              this.save('salesperson', salesperson || null);
          },
          get_salesperson: function () {
              return this.load('salesperson');
          }
      })
  })
  ```

  ```js
  // salesperson_model.js
  odoo.define('point_of_sale.salesperson_models', function (require) {
      "use strict";

      var models = require('point_of_sale.models');

      // Create get set for salesperson
      models.PosModel = models.PosModel.extend({
          get_salesperson: function () {
              if (this.db.load('pos_session_id') !== this.pos_session.id) {
                  this.set_salesperson(this.employee);
                  return this.employee;
              }

              return this.db.get_salesperson()
          },
          set_salesperson: function (employee) {
              this.set('salesperson', employee);
              this.db.set_salesperson(employee);
          }
      })

      // Extend models.Order to add new data salesperson
      var _super_order = models.Order.prototype;
      models.Order = models.Order.extend({
          export_as_JSON: function () {
              var data = _super_order.export_as_JSON.apply(this, arguments)
              data.salesperson_id = this.pos.get_salesperson().id;

              return data;
          },
          export_for_printing: function () {
              var salesperson = this.pos.get_salesperson();
              var data = _super_order.export_for_printing.apply(this, arguments)
              data.salesperson =  salesperson ? salesperson.name : "";

              return data
          }
      })

      return models
  })
  ```

  ```js
  // salesperson_gui.js
  odoo.define('point_of_sale.salesperson_gui', function (require) {
      "use strict";
      
      var gui = require('point_of_sale.gui');
      var core = require('web.core');
      var _t = core._t

      gui.Gui.include({
          select_salesperson: function(options) {
              options = options || {};
              var self = this;
              var list = [];

              this.pos.employees.forEach(function(salesperson) {
                  if (!options.only_managers || salesperson.role === 'manager') {
                      list.push({
                      'label': salesperson.name,
                      'item':  salesperson,
                      });
                  }
              });

              var prom = new Promise(function (resolve, reject) {
                  self.show_popup('selection', {
                      title: options.title || _t('Select User'),
                      list: list,
                      confirm: resolve,
                      cancel: reject,
                      is_selected: function (salesperson) {
                          return salesperson === self.pos.get_salesperson();
                      },
                  });
              });

              return prom.then(function (salesperson) {
                  return self.ask_password(salesperson.pin).then(function(){
                      return salesperson;
                  });
              });
          },
      }); 
  })
  ```

  then it can be clicked and the name of Salesperson will be loaded default to logged-in employee.

## [Task 2] Modify receipt to print both Cashier and Salesperson
1. Since all data needed have been created on Task 1, we only need to extend template OrderReceipt and replace element that have `.cashier` class. We can add this code below to  file `pos_customization.xml`
 
  ```xml
  <!-- ... -->
    <t t-extend="OrderReceipt">
      <t t-jquery=".pos-receipt .pos-receipt-contact .cashier" t-operation="replace">
        <div class='cashier'>
          <div>--------------------------------</div>
          <div>Cashier: <t t-esc='receipt.cashier' /></div>
          <div>Salesperson: <t t-esc="receipt.salesperson" /></div>
        </div>
      </t>
    </t>
  </template>
  ```

## [Task 3] Add Salesperson to pos.order form view and list view
1. First, create a model called `pos_order.py` and fill it with

  ```py
  from odoo import models, fields, api

  class PosOrder(models.Model):
      _inherit = "pos.order"

      salesperson_id = fields.Many2one('hr.employee')

      @api.model
      def _order_fields(self, ui_order):
          order_fields = super(PosOrder, self)._order_fields(ui_order)
          order_fields['salesperson_id'] = ui_order.get('salesperson_id', False),

          return order_fields

  ```

2. Then, we need to create views to extend form and tree view, we create file called `pos_order_salesperson_view.xml` and we fill it with
 
  ```xml
  <odoo>
    <record id="view_pos_order_salesperson_form" model="ir.ui.view">
      <field name="name">pos.order.salesperson.form</field>
      <field name="model">pos.order</field>
      <field name="inherit_id" ref="point_of_sale.view_pos_pos_form" />
      <field name="arch" type="xml">
        <xpath expr="//field[@name='partner_id']" position="before">
          <field name="salesperson_id" string="Salesperson" />
        </xpath>
      </field>
    </record>

    <record id="view_pos_order_salesperson_tree" model="ir.ui.view">
      <field name="name">pos.order.salesperson.tree</field>
      <field name="model">pos.order</field>
      <field name="inherit_id" ref="point_of_sale.view_pos_order_tree" />
      <field name="arch" type="xml">
        <xpath expr="//field[@name='amount_total']" position="before">
          <field name="salesperson_id" string="Salesperson" />
        </xpath>
      </field>
    </record>
  </odoo>
  ```

  and Salesperson will be shown on pos.order list view and form view

## [Task 4] Add Salesperson to report.pos.order Filter and Group By
1. First, we need to create model called `report_pos_order.py` and extend from model `report.pos.order`, we need to extend function select and group_by so it will get salesperson_id from database. Fill the model with
 
  ```py
  from odoo import models, fields, api

  class ReportPosOrder(models.Model):
      _inherit = 'report.pos.order'

      salesperson_id = fields.Many2one('hr.employee')

      def _select(self):
          select = super(ReportPosOrder, self)._select()
          select = select + ", s.salesperson_id AS salesperson_id"

          return select

      def _group_by(self):
          group_by = super(ReportPosOrder, self)._group_by()
          group_by = group_by + ", s.salesperson_id"

          return group_by

  ```

2. Then we need to extend `view_report_pos_order_search`, create file called `report_pos_order_salesperson_search.xml` and put it inside `views` directory, then fill it with code below
  
  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <odoo>
      <record id="view_report_pos_order_search_salesperson" model="ir.ui.view">
          <field name="name">report.pos.order.search.salesperson</field>
          <field name="model">report.pos.order</field>
          <field name="inherit_id" ref="point_of_sale.view_report_pos_order_search"/>
          <field name="arch" type="xml">
              <xpath expr="//filter[@name='User']" position="after">
                  <filter string="Salesperson" name="salesperson_id" domain="[]" context="{'group_by':'salesperson_id'}" />
              </xpath>
          </field>
      </record>
  </odoo>
  ```

## [Task 5] Change POS logo from default Odoo to company logo
1. First, we create new file called `customization_widget.js` and create new widget with name `CustomizationWidget`. then we fill it with
  
  ```js
  odoo.define('point_of_sale.customization_widget', function (require) {
      "use strict";

      var chrome = require('point_of_sale.chrome');
      var PosBaseWidget = require('point_of_sale.BaseWidget');
      var screens = require('point_of_sale.screens');

      // widget logo
      var CustomizationWidget = PosBaseWidget.extend({
          template: "CustomizationWidget",
          init: function (parent, options) {
              options = options || {};
              this._super(parent, options);
          },
          get_companylogo: function () {
              var company_logo = this.pos.company_logo.src;

              // Fallback to POS default logo
              return company_logo ? company_logo : "/point_of_sale/static/src/img/logo.png";
          }
      })
      chrome.Chrome.include({
          build_widgets: function () {
              this.widgets.push({
                  'name': 'customization',
                  'widget': CustomizationWidget,
                  'replace': ".placeholder-CustomizationWidget"
              })
              this._super();
          }
      })
      
      return {
          'CustomizationWidget': CustomizationWidget,
      }
  })
  ```

  it is the same as when we creating SalespersonWidget, the difference is we only need to create Widget to get the logo from database, since it have been loaded to model js
2. Then create file called `pos_customization.xml` inside `/static/src/xml/` and fill it with
 
  ```xml
  <template id="template" xml:space="preserve">

    <!-- Change logo from POS page -->
    <t t-extend="Chrome">
      <t t-jquery=".pos > .pos-topheader > .pos-branding > .pos-logo" t-operation="replace">
        <span class="placeholder-CustomizationWidget"></span>
      </t>
    </t>

    <t t-name="CustomizationWidget">
      <img class="pos-logo" t-att-src="widget.get_companylogo()" alt="Logo"/>
    </t>
  </template>
  ```

## [Task 6] Add accent color to topheader, selected product, and selected input mode
1. For this task, we only need to create a CSS file, and find the style from base point_of_sale module, then we change some of them. Create new file called `pos_customization.css` and don't forget to add it to templates, then fill it with
 
  ```css
  /* Button selected mode Qty/Disc/Price */
  .pos .mode-button.selected-mode {
    color: white;
    background: #000080;
    border-color: transparent;
  }
  .pos .mode-button.selected-mode:hover {
    background: #000080;
    color: white;
    border-color: transparent;
  }

  /* Orderline */
  .pos .order .orderline.selected {
    background: #000080;
    color: white;
    -webkit-transition: background 250ms ease-in-out;
    -moz-transition: background 250ms ease-in-out;
    transition: background 250ms ease-in-out;
    cursor: default;
  }
  .pos .order .orderline.selected .info-list {
    color: #b9b9b9;
  }
  .pos .order .orderline.selected .info-list em {
    color: #c1c1c1;
  }

  /* Topheader */
  .pos .pos-topheader {
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 48px;
    margin: 0;
    padding: 0;
    color: white;
    background: #000080;
  }
  .pos .pos-topheader .pos-branding .pos-top-rightheader > * {
    border-right: 0;
    border-left: 1px solid #292929;
  }
  .pos-top-rightheader .oe_status {
    padding: 18px;
    color: white;
  }
  .pos-top-rightheader .oe_status:hover {
    color: rgba(255, 255, 255, 0.6);
  }
  ```

## [Task 7] Add thumbnail image to the left of the product on orderline
1. We need to extend model OrderLine and add new function called `get_image_128`. Create file `customization_model.js` inside `static/src/js/` and fill it with
 
  ```js
  odoo.define('point_of_sale.customization_models', function (require) {
      "use strict";

      var models = require('point_of_sale.models');

      models.Orderline = models.Orderline.extend({
          get_image_128: function (product) {
              return window.location.origin + '/web/image?model=product.product&field=image_128&id='+product.id;
          }
      })

      return models;
  })
  ```

2. Then we need replace existing Orderline widget, go to file `pos_customization.xml`, and add code below to the file
 
  ```xml
  <t t-inherit="point_of_sale.Orderline" t-inherit-mode="extension">
    <xpath expr="li" position="replace">
      <li t-attf-class="orderline #{ line.selected ? 'selected' : '' }">
        <div class="row">
          <div class="product-image">
            <img t-att-src="line.get_image_128(line.get_product())" />
          </div>
          <div class="product-detail">
            <span class="product-name">
                <t t-esc="line.get_product().display_name"/>
                <t t-if="line.get_product().tracking!=='none'">
                    <i class="oe_link_icon fa fa-list oe_icon line-lot-icon oe_green" t-if="line.has_valid_product_lot()"
                        aria-label="Valid product lot" role="img" title="Valid product lot"/>
                    <i class="oe_link_icon fa fa-list oe_icon line-lot-icon oe_red" t-if="!line.has_valid_product_lot()"
                        aria-label="Invalid product lot" role="img" title="Invalid product lot"/>
                </t>
            </span>
            <span class="price">
                <t t-esc="widget.format_currency(line.get_display_price())"/>
            </span>
            <ul class="info-list">
                <t t-if="line.get_quantity_str() !== '1' || line.selected ">
                    <li class="info">
                        <em>
                            <t t-esc="line.get_quantity_str()" />
                        </em>
                        <t t-esc="line.get_unit().name" />
                        at
                        <t t-if="line.display_discount_policy() == 'without_discount' &amp;&amp;
                            line.get_unit_display_price() != line.get_lst_price()">
                            <s>
                                <t t-esc="widget.format_currency(line.get_fixed_lst_price(),'Product Price')" />
                            </s>
                            <t t-esc="widget.format_currency(line.get_unit_display_price(),'Product Price')" />
                        </t>
                        <t t-else="">
                            <t t-esc="widget.format_currency(line.get_unit_display_price(),'Product Price')" />
                        </t>
                        /
                        <t t-esc="line.get_unit().name" />
                    </li>
                </t>
                <t t-if="line.get_discount_str() !== '0'">
                    <li class="info">
                        With a
                        <em>
                            <t t-esc="line.get_discount_str()" />%
                        </em>
                        discount
                    </li>
                </t>
            </ul>
          </div>
        </div>
      </li>
    </xpath>   
  </t>
  ```

3. Then we need to change some styling, go to `pos_customization.css` and add code below
 
  ```css
  /* Product Image */
  .pos .order .orderline .row {
    display: flex;
    padding: 5px;
  }
  .pos .order .orderline .row .product-image img {
    max-height: 80px;
    max-width: 60px;
  }
  .pos .order .orderline .row .product-detail {
    margin-left: 15px;
    flex-grow: 2;
  }
  ```

## [Task 8] Add Clear Current Order button
1. We need to extend `ProductScreenWidget`, and add the button there. Go to file `pos_customization.xml` and add code below
 
  ```xml
  <!-- Clear Current Order button -->
  <t t-inherit="point_of_sale.ProductScreenWidget" t-inherit-mode="extension">
    <xpath expr="//div[hasclass('placeholder-ActionpadWidget')]" position="before">
      <div class="clear-current-order">
        <button class="button clearcurrentorder-button">Clear Current Order</button>
      </div>
    </xpath>
  </t>
  ```

2. Then add some styling, go to `pos_customization.css` and add code below
 
  ```css
  /* Clear current order button */
  .pos .clear-current-order .button {
    position: relative;
    display: block;
    height: 54px;
    width: 186px;
    margin: 16px;
  }
  .pos .clear-current-order .button:hover {
    background: #efefef;
  }
  .pos .clear-current-order .button:active {
    background: black;
    border-color: black;
    color: white;
  }
  ```

  Until this step, the button have style but it won't do anything when clicked, we need to add click handler to the button
3. Go to file `customizaiton_widget.js`, and import `point_of_sale.screens` to extends `renderElement` from `ProductScreenWidget`. Then add code below
 
  ```js
  // extend ProductScreenWidget to add "clear current order" button
  screens.ProductScreenWidget.include({
      renderElement: function () {
          var self = this;
          this._super();

          this.$('.clearcurrentorder-button').click(function () {
              var order = self.pos.get_order();
              if (order.orderlines.length == 0) {
                  return;
              }

              self.pos.delete_current_order();

          })
      }
  })
  ```
  
  There we add click handler to the button, when the ordeerlines is not empty, it run the `delete_current_order()` function, clear the current order, and increment the order id.