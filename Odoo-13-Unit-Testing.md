# Odoo 13 Unit Test

Odoo already provided unit testing and integration testing to work with. 

## Some BaseCase that we can use

We can inherit all class below to run the test cases. classes below inherited from BaseCase, 
so it will automatically tagged as 'standard', which will be automatically run when we add
the `--test-enable` flag. We can add to tagged decorator with '-standard'

### TransactionCase

Very common used Case is TransactionCase. This will run each test method in its own 
transaction and with its own cursor, and the transaction is rolled back after each test.

Say we have method create for model category that will make sure every category code is unique. 
Since this case will create it's own transaction on each test, so if we create 2 category that have
the same code in the same test method, it will throw the error. But if we create 
each category in each of the test cases, it won't throw the error since each of 
the category is on the different transaction.

### SingleTransactionCase

The difference with TransactionCase above is it will create single transaction for all method in the class.
So if above case implemented here, it will throw the error even with different method, 
because it is happen in the same transaction and cursor.

### SavepointCase
Basically, this SavepointCase is almost the same as SingleTransactionCase which run in their respective transaction. 
But the difference is SavepointCase run on a sub-transaction and will be rolled-back when the test method already run. 

Say we have some methods that will calculate the tax from products. There is different type of tax here and it need 
fresh products data to correctly calculated since the tax type here can't be merge. For this case, TransactionCase is out of the game. 
It can use SingleTransactionCase but the downside is it need to populate the products again for every type of tax that tested.
So for this cases, SavepointCase is the best choice to use, since it can populate the products in a transaction, 
then every test cases run in a sub-transaction that will be rolled-back when the test is complete and continue with the next 
test cases that need the products data fresh without changed from the previous test cases.

For example, we can see it from module 
[account when it setup the transaction](https://github.com/odoo/odoo/blob/13.0/addons/account/tests/account_test_savepoint.py#L26-L170) 
and when it [test some cases to create account_payment](https://github.com/odoo/odoo/blob/13.0/addons/account/tests/test_account_payment.py), every test will be run with fresh account. 

### HttpCase

Basically this case is inherited from TransactionalCase, but it can run headless 
chrome that can be used to test the frontend too. 

For HttpCase example, there is unit test that run 
[PoS tours1](https://github.com/odoo/odoo/blob/13.0/addons/point_of_sale/tests/test_point_of_sale_ui.py) and 
[PoS tours2](https://github.com/odoo/odoo/blob/13.0/addons/point_of_sale/tests/test_frontend.py) that will run the tours, 
and there is other example for HttpCase for [payment_paypal that render the xml and programatically get the data rendered](https://github.com/odoo/odoo/blob/13.0/addons/payment_paypal/tests/test_paypal.py#L53-L82)

For the payment_paypal, even it extend HttpCase from AccountingTestCase, this render can be run with TransactionalCase since the 
render things run in python. Meanwhile for PoS tours, it create new instance of phantom_js and all unit test run is from js, so when it want
to signal that the test is success, just call console.log('test successful') and to signal when the test failure, just raise an exception or 
call console.error.

## some assert method that we can use

Meanwhile some point in section above used to how the test run with their respective Cases. In this section, 
we can use some [method provided by unittest python](https://docs.python.org/3.7/library/unittest.html#unittest.TestCase.debug) 
to assert the method we test return data as we expected.

For python3.7, we have some of this method:

| Method | Check that | Notes | 
| ------ | ---------- | ----------------- |
| `assertEqual(real_data, expected_data)` | `real_data == expected_data` | |
| `assertNotEqual(real_data, expected_data)` | `real_data != expected_data` | |
| `assertTrue(real_data)` | `bool(real_data) == True` | type data of real_data don't need to be boolean; if you want to check that real_data data type is boolean and it is True, then use `assertIs(real_data, True)` |
| `assertFalse(real_data)` | `bool(real_data) == False` | |
| `assertIs(real_data, expected_data)` | `real_data is expected_data` | |
| `assertIsNot(real_data, expected_data)` | `real_data is not expected_data` | |
| `assertIsNone(real_data)` | `real_data is None` | |
| `assertIsNotNone(real_data)` | `real_data is not None` | |
| `assertIn(real_data, expected_data)` | `real_data in expected_data` | |
| `assertNotIn(real_data, expected_data)` | `real_data not in expected_data` | |
| `assertIsInstance(object, class)` | `isinstance(object, class)` | check if object is instance from the class |
| `assertIsNotInstance(object, class)` | `not isinstance(object, class)` | check if object is not instance from the class |

And other than 'usual' assert above, there is one again that very useful, called `assertRaises(exception)`. 
assertRaises will assert if that method call an exception. To see the example for this method, you can see on hr_loan_test below that will raise a ValidationError, when the exception raised, the assert will success. But if no exception raised, it will thrown failed.

We use first parameter as real_result from the method or something that we want to test, and second parameter as our expected result for the first parameter to pass that assertion.

## Testing modules that create record on model

### Setup

#### Folder Structure

First we need to create new directory called `tests` in root custom-module, 
then we create new file `__init__.py` and `test_<model_name>.py`.
Say we have module `hr_loan_management` that create new model to save the employee loan,
so simple structure will be like this.

```sh
.
├── __init__.py
├── __manifest__.py
├── data
│   └── hr_loan_sequence.xml
├── models
│   ├── __init__.py
│   ├── hr_loan.py
├── tests
│   ├── __init__.py
│   └── test_hr_loan.py
└── views
    ├── hr_loan_views.xml
    └── hr_views.xml
```
> - The prefix `test_` in the file name is required.
> - Do not import tests directory to main init file.

#### Create TestCase

Then, we open the file `test_hr_loan.py` and we import `TransactionCase` and `tagged` from `odoo.tests`. 
This tagged decorator is to specify the tags to run it specifically.

Then create new class that inherit `TransactionCase`, so it would be like
```py
from odoo.tests import TransactionCase, tagged

@tagged('-standard', 'hrloan')
class TestHrLoan(TransactionCase):
    def setUp(self):
        # define the employee and loan record here
        super(TestHrLoan, self).setUp()
        loan_manager = self.env.ref('pcs_employee_loan_management.group_hr_loan_manager')

        self.manager = self.env['res.users'].create({
            'name': 'Loan Manager',
            'login': 'loan_manager',
            'email': 'loan_manager@example.com',
            'signature': '--\nLoaan',
            'notification_type': 'email',
            'groups_id': [(6, 0, [loan_manager.id, self.env.ref('base.group_user').id])]
        })

        self.user = self.env['res.users'].create({
            'name': 'Employee Test',
            'login': 'employee_tes',
            'email': 'employee_test@example.com',
            'signature': '--\Employee Test',
            'notification_type': 'email',
            'groups_id': [(6, 0, [self.env.ref('hr.group_hr_user').id])]
        })

        self.employee = self.env['hr.employee'].create({
            'name': self.user.name,
            'user_id': self.user.id,
        })

        self.loan_01 = {
            'loan_amount': 120000,
            'installment': 6,
        }

        self.loan_02 = {
            'loan_amount': 360000,
            'installment': 36,
        }

    def test_create_loan(self):
        # create the loan and assert the record here
        loan = self.env['hr.loan'].with_user(self.user).create(self.loan_01)
        # check if record created
        self.assertIs(type(loan.id), int, "loan.id can't be find; loan not created")
        # check if state is draft
        self.assertEqual(loan.state, 'draft', "state is no equal as draft as it should be when first created")
        # submit loan request; the state should be waiting_approval
        loan.action_submit()
        self.assertEqual(loan.state, 'waiting_approval', "state is not equal with waiting_approval")
        # approve the loan request; triggered by employee, it should not changing the state to approve
        loan.action_approve()
        self.assertNotEqual(loan.state, 'approve', "state is not changed to 'approve'")
        # get the loan using manager credentials
        loan_by_manager = self.env['hr.loan'].with_user(self.manager).browse(loan.id)
        # approve the loan request; triggered by loan manager, it should changed the state to approve
        loan_by_manager.action_approve()
        self.assertEqual(loan.state, 'approve', "state is not 'approve', the approve button is not right")

        # Test to create loan2; this should raise ValidationError 
        # because there is existing loan for that employee that still run
        with self.assertRaises(ValidationError) as loan2_raise:
            self.env['hr.loan'].with_user(self.user).create(self.loan_02)
        _logger.info(loan2_raise.exception)

```

And create another file called `test_hr_loan.py` and put it inside `__init__.py` to see `SingleTransactionCase` works.
```py
from odoo.tests import SingleTransactionCase, tagged

@tagged('-standard', 'hrloan')
class TestHrLoanSingle(SingleTransactionCase):
    def setUp(self):
        # define the employee and loan record here
        super(TestHrLoanSingle, self).setUp()
        loan_manager = self.env.ref('pcs_employee_loan_management.group_hr_loan_manager')

        self.manager = self.env['res.users'].create({
            'name': 'Loan Manager',
            'login': 'loan_manager',
            'email': 'loan_manager@example.com',
            'signature': '--\nLoaan',
            'notification_type': 'email',
            'groups_id': [(6, 0, [loan_manager.id, self.env.ref('base.group_user').id])]
        })

        self.user = self.env['res.users'].create({
            'name': 'Employee Test',
            'login': 'employee_tes',
            'email': 'employee_test@example.com',
            'signature': '--\Employee Test',
            'notification_type': 'email',
            'groups_id': [(6, 0, [self.env.ref('hr.group_hr_user').id])]
        })

        self.employee = self.env['hr.employee'].create({
            'name': self.user.name,
            'user_id': self.user.id,
        })

        self.loan_01 = {
            'loan_amount': 120000,
            'installment': 6,
        }

        self.loan_02 = {
            'loan_amount': 360000,
            'installment': 36,
        }

    def test_create_loan(self):
        # create the loan and assert the record here
        loan = self.env['hr.loan'].with_user(self.user).create(self.loan_01)
        # check if record created
        self.assertIs(type(loan.id), int, "loan.id can't be find; loan not created")
        # check if state is draft
        self.assertEqual(loan.state, 'draft', "state is no equal as draft as it should be when first created")
        # submit loan request; the state should be waiting_approval
        loan.action_submit()
        self.assertEqual(loan.state, 'waiting_approval', "state is not equal with waiting_approval")
        # approve the loan request; triggered by employee, it should not changing the state to approve
        loan.action_approve()
        self.assertNotEqual(loan.state, 'approve', "state is not changed to 'approve'")
        # get the loan using manager credentials
        loan_by_manager = self.env['hr.loan'].with_user(self.manager).browse(loan.id)
        # approve the loan request; triggered by loan manager, it should changed the state to approve
        loan_by_manager.action_approve()
        self.assertEqual(loan.state, 'approve', "state is not 'approve', the approve button is not right")

        # Test to create loan2; this should raise ValidationError 
        # because there is existing loan for that employee that still run
        with self.assertRaises(ValidationError) as loan2_raise:
            self.env['hr.loan'].with_user(self.user).create(self.loan_02)
        _logger.info(loan2_raise.exception)
    
    def test_create_loan2(self):
        # loan2 created here too, to see that SingleTransactionCase will do all the test in 1 transaction
        with self.assertRaises(ValidationError) as loan2_raise:
            self.env['hr.loan'].with_user(self.user).create(self.loan_02)
        _logger.info(loan2_raise.exception)

```
Decorator tagged above means we want to remove it from running the test by default 
(use TestCase from odoo.tests means tag 'standard' passed to the class except we 
explicitly remove it by using prefix '-').
It will only run the test when test-tags specified in the flag.

### Run the TestCase

So we already created the testcase, and now it is time to run the test. To run this, 
make sure you are running this with empty database, maybe new database that freshly created.

Here i using conf file that already save my db credentials and addons_path, so we only need 
to add some flag to specify the db-name and to enable the test, below is my `test.conf` file
```py
[options]
db_user = akhmad
db_password = False
db_name = False
addons_path = ./addons, ./odoo/addons, ./enterprise, ./custom-addons
```
and run it with command
```sh
python3 odoo-bin -c test.conf -d odoo13_test --test-enable --test-tags 'hrloan' -i hr_loan_management --stop-after-init
```
command above is, we use test.conf as config file, and use database odoo13_test, enable test with only run test that have tagged as 'hrloan', and initialize 'hr_loan_management' and will stop the server after initialize.

We only need to initialize our custom module and it will initialize module that filled in depends in manifest.
then it will run the test with only tags that we pass and it will close the server after the test is complete.


## Testing module that extend base model

### Setup

#### Folder Structure

First we need to create new directory called `tests` in root custom-module, 
then we create new file `__init__.py` and `test_<model_name>.py`. 
So if we want to create unit test for our custom module that extend model `sale.order`, 
we will have structure like this.

```sh
.
├── __init__.py
├── __manifest__.py
├── models
│   ├── __init__.py
│   └── sale_order.py
├── tests
│   ├── __init__.py
│   └── test_sale_order_approval.py
└── views
    └── sale_order_views.xml

```
> - The prefix `test` in the file name is required.
> - Do not to import tests directory to main init file.

#### Create TestCase

For the module `sale_order_approval`, we extend the sale order to always 
need approval from sales manager, so we add new state and there is new button that 
can be called by sales manager only. Since this module extend `sale.order`, we can go to 
the base module `sale` and to tests directory, [there is common test that can be inherited](https://github.com/odoo/odoo/blob/13.0/addons/sale/tests/test_sale_common.py), for this case, we don't need 
to define setUp again and create new record for testing like example above.

Here we can import the TestSale from that file, and inherit it, and create the test cases.
```py
from odoo.addons.sale.tests.test_sale_common import TestSale
from odoo.tests import tagged

@tagged('-standard', 'sale_order_approval')
class TestSaleOrderApproval(TestSale):
    def test_approval_salesman(self):
        """ Test draft approval salesman 
        this test should Fail since only manager can approve """
        for product in self.products.values():
            sale_order_vals = {
                'partner_id': self.partner.id,
                'partner_invoice_id': self.partner.id,
                'partner_shipping_id': self.partner.id,
                'order_line': [(0, 0, {
                    'name': product.name,
                    'product_id': product.id,
                    'product_uom_qty': 2,
                    'product_uom': product.uom_id.id,
                    'price_unit': product.list_price,
                })],
                'pricelist_id': self.env.ref('product.list0').id,
            }

        so_salesman = self.env['sale.order'].with_user(self.user).create(sale_order_vals)
        so_salesman.action_draft()
        self.assertTrue(so_salesman.state == 'prepare', 'Sale Order failed to change to prepare for approval')

        # This approve should fail
        so_salesman.action_approve()
        self.assertFalse(so_salesman.state == 'draft', "Sale Order cannot be approved by salesman")

    def test_approval_manager(self):
        """ Test draft approval manager
        this test should True since this is run by manager """
        for product in self.products.values():
            sale_order_vals = {
                'partner_id': self.partner.id,
                'partner_invoice_id': self.partner.id,
                'partner_shipping_id': self.partner.id,
                'order_line': [(0, 0, {
                    'name': product.name,
                    'product_id': product.id,
                    'product_uom_qty': 2,
                    'product_uom': product.uom_id.id,
                    'price_unit': product.list_price,
                })],
                'pricelist_id': self.env.ref('product.list0').id,
            }

        so_manager = self.env['sale.order'].with_user(self.manager).create(sale_order_vals)
        so_manager.action_draft()
        self.assertTrue(so_manager.state == 'prepare', 'Sale Order failed to change to prepare for approval')

        # this approve should success
        so_manager.action_approve()
        self.assertTrue(so_manager.state == 'draft', "Sale order can be approved by sales manager")
```

### Run the TestCase

Same as example above, since we put `-standard` to decorator tagged, 
then we need to explicitly call the tag. With the same `test.conf`, 
we just need to change the `--test-tags` flag and `-i` flag. So the command will be
```sh
python3 odoo-bin -c test.conf -d odoo13_test --test-enable --test-tags 'sale_order_approval' -i hr_loan_management --stop-after-init
```